from flask import Flask, jsonify
from flask import Response


def create_app():
    app = Flask(__name__)
    app.config['TESTING'] = True
    return app


app = create_app()


@app.route("/")
def index():
    return "Hello world!"


@app.route('/ping')
def ping():
    return jsonify({'ping':'pong'})


if __name__ == '__main__':
    app.run()